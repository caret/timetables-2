'''
Created on Aug 1, 2012

@author: ieb
'''
from django.shortcuts import render
from timetables.model.models import User, \
    Classifier, TermInfo, Location, Event, EventSeries, EventType

import logging
from copy import deepcopy
from timetables.administrator.views import AdminBaseView
from timetables.utils.http import ContextKeys, HttpParameters
from timetables.utils import TimetablesPaginator
log = logging.getLogger(__name__)
del logging


class AllEvents(AdminBaseView):
    '''
    View to list all events.
    '''

    PAGE_STATE_KEYS = (
                    ("e","internal_external"),
                    ( "lecturer", "filter_settings_lecturer_selected" ),
                    ( "term", "filter_settings_term_selected"),
                    ( "location", "filter_settings_location_selected" ),
                    ( "type", "filter_settings_type_selected"),
                    ( "level", "filter_settings_level_selected"),
                    ( "subject", "filter_settings_subject_selected"),
                    ( "q", "search_term"),
                    )


    results_per_page = 20

    ORDER_FIELDS = {
        'stitle' : 'owning_series__title',
        'gtitle' : 'owning_series__owning_group__name',
        'date' : 'default_timeslot__start_time',
        'lastupdate' : 'owning_series__last_modified'
        }
    ORDER_SERIES_FIELDS = {
        'stitle' : 'title',
        'gtitle' : 'owning_group__name',
        'lastupdate' : 'last_modified'
        }

    CLASSIFIER_TYPES = {
        'S': 'Subject',
        'L': 'Level'
        }

    def js_main_module(self):
        return "admin/events"

    
    def list_events(self, request, with_sort=True):
        '''
        Build the events query.
        :param request:
        :param with_sort:
        '''
        if self.org is not None:
            querySet = Event. \
                objects.filter(owning_series__owning_group__owning_organisation=self.org)
        else:
            querySet = Event.objects.all()
        if self.year is not None:
            querySet = querySet.filter(owning_series__owning_group__term__academic_year=self.year)
        if "q" in request.GET:
            querySet = querySet.filter(owning_series__title__icontains=request.GET['q'].lower())
        full_query_set = deepcopy(querySet)
        if "lecturer" in request.GET and request.GET['lecturer'] != "0":
            crsid = request.GET['lecturer'].split(" ")
            querySet = querySet.filter(owning_series__associated_staff__username=crsid)
        if "term" in request.GET and request.GET['term'] != "0":
            querySet = querySet.filter(owning_series__owning_group__term__term_info__name=request.GET['term'])
        if "location" in request.GET and request.GET['location'] != "0":
            querySet = querySet.filter(owning_series__location__name=request.GET['location'])
        if "type" in request.GET and request.GET['type'] != "0":
            querySet = querySet.filter(owning_series__owning_group__event_type__name=request.GET['type'])
        if "level" in request.GET:
            querySet = querySet.filter(owning_series__owning_group__groupusage__classifiers__id__in=request.GET.getlist("level"))
        if "subject" in request.GET:
            querySet = querySet.filter(owning_series__owning_group__groupusage__classifiers__id__in=request.GET.getlist("subject"))
        
        # Apply sorting 
        if with_sort:
            if "s" in request.GET and request.GET['s'] in AllEvents.ORDER_FIELDS:
                ordering = AllEvents.ORDER_FIELDS[request.GET["s"]]
                if "o" in request.GET and request.GET['o'] == 'd':
                    ordering = "-%s" % ordering
                querySet = querySet.order_by(ordering)
            querySet.order_by("id")
        return querySet, full_query_set

    def list_event_series(self, request, with_sort=True):
        '''
        Build the event series query.
        :param request:
        :param with_sort:
        '''
        if self.org is not None:
            querySet = EventSeries. \
                objects.filter(owning_group__owning_organisation=self.org).exclude(external_url=None)
        else:
            querySet = Event.objects.all()
        if self.year is not None:
            querySet = querySet.filter(owning_group__term__academic_year=self.year)
        if "q" in request.GET:
            querySet = querySet.filter(title__icontains=request.GET['q'].lower())
        full_query_set = deepcopy(querySet)

        if "lecturer" in request.GET and request.GET['lecturer'] != "0":
            crsid = request.GET['lecturer'].split(" ")
            querySet = querySet.filter(associated_staff__username=crsid)
        if "term" in request.GET and request.GET['term'] != "0":
            querySet = querySet.filter(owning_group__term__term_info__name=request.GET['term'])
        if "location" in request.GET and request.GET['location'] != "0":
            querySet = querySet.filter(location__name=request.GET['location'])
        if "type" in request.GET and request.GET['type'] != "0":
            querySet = querySet.filter(owning_group__event_type__name=request.GET['type'])
        if "level" in request.GET:
            querySet = querySet.filter(owning_group__groupusage__classifiers__in=request.GET.getlist("level"))
        if "subject" in request.GET:
            querySet = querySet.filter(owning_group__groupusage__classifiers__in=request.GET.getlist("subject"))

        # Apply sorting
        if with_sort:
            if HttpParameters.SORT in request.GET and request.GET[HttpParameters.SORT] in AllEvents.ORDER_SERIES_FIELDS:
                ordering = AllEvents.ORDER_SERIES_FIELDS[request.GET[HttpParameters.SORT]]
                if HttpParameters.SORT_ORDER in request.GET and request.GET[HttpParameters.SORT_ORDER] == HttpParameters.DESC:
                    ordering = "-%s" % ordering
                querySet = querySet.order_by(ordering)
            querySet.order_by("id")
        return querySet, full_query_set

    def get_title(self, event):
        '''
        Get the title for the event taking into account overrides that might be applied.
        :param event:
        '''
        return event.owning_series.title if event.overridden_title is None \
                          else "%s (was: %s)" % ( event.overridden_title, event.owning_series.title )

    def get_owning_group(self, event):
        '''
        Get the owning group of the event.
        :param event:
        '''
        return event.owning_series.owning_group

    def get_location(self, event):
        '''
        Get the location of the event taking into account any overrides.
        :param event:
        '''
        return event.owning_series.location.name if event.overridden_location is None \
                          else "%s (was: %s)" % ( event.overridden_location.name, event.owning_series.location.name)

    def get_date(self, event):
        '''
        Get the date for the event.
        :param event:
        '''
        return event.default_timeslot

    def get_last_update(self, event):
        '''
        Get when the event was last updated.
        :param event:
        '''
        return max([event.owning_series.last_modified, event.last_modified])

    def get_classification(self, event):
        '''
        Get classifications for the event.
        :param event:
        '''
        # There is no navigable route from eventseries to classification which will be bad in production as it will not be possible
        # to fetch related items. This will ad 40 queries to the set required here. This solution only selects the first subject
        # there may be more. We also have no idea how level and subject relate to one another.
        if isinstance(event, Event):
            selector = event.owning_series.owning_group
        else:
            selector = event.owning_group
        classifiers = {}
        if not hasattr(self, "_classifier_cache"):
            self._classifier_cache = {}
        if selector in self._classifier_cache:
            return self._classifier_cache[selector]
        for c in Classifier.objects.filter(groupusage__group=selector).prefetch_related("group"):
            if c.group.family in AllEvents.CLASSIFIER_TYPES:
                classifiers[AllEvents.CLASSIFIER_TYPES[c.group.family]] = c.value
            else:
                classifiers[c.group.family] = c.value
        if 'Level' in classifiers and 'subject' in classifiers:
            classifiers = ":".join([classifiers['Level'],classifiers['Subject']])
        elif 'Level' in classifiers:
            classifiers = classifiers['Level']
        else:
            classifiers = ''
        self._classifier_cache[selector] = classifiers
        return classifiers

    def get_level(self, event):
        '''
        Get the level of the event.
        
        :param event:
        '''
        if isinstance(event, Event):
            selector = event.owning_series.owning_group
        else:
            selector = event.owning_group
        if not hasattr(self, "_level_cache"):
            self._level_cache = {}
        if selector in self._level_cache:
            return self._level_cache[selector]
        levels = ",".join([ c.value for c in Classifier.objects.filter(groupusage__group=selector,group__family='L').prefetch_related("group")])
        self._level_cache[selector] = levels
        return levels
    
    def get_subject(self, event):
        '''
        Get the subhects associated with the even. This method will cache to be efficient.
        :param event:
        '''
        if isinstance(event, Event):
            selector = event.owning_series.owning_group
        else:
            selector = event.owning_group
        if not hasattr(self, "_subject_cache"):
            self._subject_cache = {}
        if selector in self._subject_cache:
            return self._subject_cache[selector]
        subject = ",".join([ c.value for c in Classifier.objects.filter(groupusage__group=selector,group__family='S').prefetch_related("group")])
        self._subject_cache[selector] = subject
        return subject


    # A wrapper function for the leturer column. It fetches the full name for the
    # lecturer (user), but if that is empty then returns the username instead.
    def _get_lecturer_name(self, user):
        full_name = user.get_full_name()
        if ( full_name == '' ):
            return user.username
        else:
            return full_name

    def get_lecturers(self, event):
        '''
        Get a list of lecturers for the event.
        :param event:
        '''
        if event.is_staff_overridden:
            return map( self._get_lecturer_name, event.overridden_staff.all() )
        else:
            return map( self._get_lecturer_name, event.owning_series.associated_staff.all() )


    def context(self, request, based_on=None):
        '''
        Create a context for the list of events.
        :param request:
        :param based_on:
        '''
        context = super(AllEvents, self).context(request, based_on=based_on)
        if "v" in request.GET and request.GET['v'] == 'e':
            self._external_events_context(request,context)
            context["internal_external"] = "e"
        else:
            self._internal_events_context(request,context)


        # pass sort quey and order to the template
        if HttpParameters.SEARCH_QUERY in request.GET:
            context[ContextKeys.SEARCH_QUERY] = request.GET[HttpParameters.SEARCH_QUERY]
        if HttpParameters.SORT in request.GET:
            context[ContextKeys.SORT] = request.GET[HttpParameters.SORT]
        if HttpParameters.SORT_ORDER in request.GET:
            context[ContextKeys.SORT_ORDER] = request.GET[HttpParameters.SORT_ORDER]

        context["url_params"] = {
                "view": "admin events",
                "params": [
                    self.year.url_id(),
                    self.org.url_id()
                ]
            }

        context["page_title"] = "Event Timetables"
        # All query parameters that influence paging, these will be saved by
        context["page_state_keys"] = AllEvents.PAGE_STATE_KEYS

        # Support event publication messages.
        if 'evp' in request.GET:
            context['published_timetables'] = request.GET['evp']
        if 'eevp' in request.GET:
            context['embargo_published_timetables'] = request.GET['eevp']
            context['embargo_date'] = request.GET['ed']

        if 'fb_u' in request.GET:
            context['updated_events'] = request.GET['fb_u']
        if 'fb_d' in request.GET:
            context['deleted_events'] = request.GET['fb_d']

        return context

    def get(self, request, year_start, org_code):
        self.locate_organisation(request, year_start, org_code)
        context = self.context(request)

        return render(request, "administrator/1.0-All-Events.html", context)
    
    def _load_filter(self, request, context, full_query_set):
        '''
        Load filters into the context based on the request and what is in the query.
        :param request:
        :param context:
        :param full_query_set:
        '''
        context["filter_settings_lecturers"] = [ user.username
                                                   for user in User.objects.filter(id__in=User.objects.
                                                        filter(eventseries__event__in=full_query_set)).
                                                        order_by("username")]
        context["filter_settings_terms"] = [ term.name
                                               for term in TermInfo.objects.
                                                    filter(id__in=TermInfo.objects.
                                                           filter(term__eventgroup__owned_series__event__in=full_query_set)).
                                                    order_by("name")]
        context["filter_settings_locations"] = [ loc.name
                                                   for loc in Location.objects.
                                                        filter(id__in=Location.objects.
                                                               filter(eventseries__event__in=full_query_set)).
                                                        order_by("name")]
        context["filter_settings_types"] = [ etype.name
                                               for etype in EventType.objects.filter(id__in=EventType.objects.
                                               filter(eventgroup__eventseries__event__in=full_query_set)) ]

        context["filter_settings_levels"] = [ c.value
                                                for c in Classifier.objects.filter(group__family='L', \
                                                                groupusage__group__eventseries__event__in=full_query_set).order_by("value")]
        context["filter_settings_subjects"] = [ c.value
                                                  for c in Classifier.objects.filter(group__family='S', \
                                                                groupusage__group__eventseries__event__in=full_query_set).order_by("value")]

        context["filter_settings_lecturer_selected"] = request.GET['lecturer'] if 'lecturer' in request.GET else None;
        context["filter_settings_term_selected"] = request.GET['term'] if 'term' in request.GET else None;
        context["filter_settings_location_selected"] = request.GET['location'] if 'location' in request.GET else None;
        context["filter_settings_type_selected"] = request.GET['type'] if 'type' in request.GET else None;
        context["filter_settings_level_selected"] = [ int(i) for i in request.GET.getlist('level')] if 'level' in request.GET else None;
        context["filter_settings_subject_selected"] = [ int(i) for i in request.GET.getlist('subject')] if 'subject' in request.GET else None;

        context['filter_settings_classifications'] = {}
        context["filter_settings_classifications"]["subjects"] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value")
        context["filter_settings_classifications"]["levels"] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                           group__organisation=self.org)).order_by("value")

        timetable = []
        if 'level' in request.GET:
            timetable.append(",".join([s.value for s in Classifier.objects.filter(id__in=request.GET.getlist('level'))]))
        if 'subject' in request.GET:
            timetable.append(",".join([s.value for s in Classifier.objects.filter(id__in=request.GET.getlist('subject'))]))
        if len(timetable) > 0 :
            context['timetable'] = ":".join(timetable)

    def _load_filter_event_series(self, request, context, full_query_set):
        context["filter_settings_lecturers"] = [ user.username
                                                   for user in User.objects.filter(id__in=User.objects.
                                                        filter(eventseries__in=full_query_set)).
                                                        order_by("username")]
        context["filter_settings_terms"] = [ term.name
                                               for term in TermInfo.objects.
                                                    filter(id__in=TermInfo.objects.
                                                           filter(term__eventgroup__owned_series__in=full_query_set)).
                                                    order_by("name")]
        context["filter_settings_locations"] = [ loc.name
                                                   for loc in Location.objects.
                                                        filter(id__in=Location.objects.
                                                               filter(eventseries__in=full_query_set)).
                                                        order_by("name")]
        context["filter_settings_types"] = [ etype.name
                                               for etype in EventType.objects.filter(id__in=EventType.objects.
                                               filter(eventgroup__eventseries__in=full_query_set)) ]
        
        context["filter_settings_levels"] = [ c.value
                                                for c in Classifier.objects.filter(group__family='L', \
                                                                groupusage__group__eventseries__in=full_query_set).order_by("value")]
        context["filter_settings_subjects"] = [ c.value
                                                  for c in Classifier.objects.filter(group__family='S', \
                                                                groupusage__group__eventseries__in=full_query_set).order_by("value")]
        
        context["filter_settings_lecturer_selected"] = request.GET['lecturer'] if 'lecturer' in request.GET else None;
        context["filter_settings_term_selected"] = request.GET['term'] if 'term' in request.GET else None;
        context["filter_settings_location_selected"] = request.GET['location'] if 'location' in request.GET else None;
        context["filter_settings_type_selected"] = request.GET['type'] if 'type' in request.GET else None;
        context["filter_settings_level_selected"] = [ int(i) for i in request.GET.getlist('level')] if 'level' in request.GET else None;
        context["filter_settings_subject_selected"] = [ int(i) for i in request.GET.getlist('subject')] if 'subject' in request.GET else None;
        
        context['filter_settings_classifications'] = {}
        context["filter_settings_classifications"]["subjects"] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='S', \
                                                                             group__organisation=self.org)).order_by("value")
        context["filter_settings_classifications"]["levels"] = Classifier.objects.filter(id__in=Classifier.objects.filter(group__family='L', \
                                                                           group__organisation=self.org)).order_by("value")

        timetable = []
        if 'level' in request.GET:
            timetable.append(",".join([s.value for s in Classifier.objects.filter(id__in=request.GET.getlist('level'))]))
        if 'subject' in request.GET:
            timetable.append(",".join([s.value for s in Classifier.objects.filter(id__in=request.GET.getlist('subject'))]))
        if len(timetable) > 0 :
            context['timetable'] = ":".join(timetable)


    def _external_events_context(self, request, context):
        '''
        Load filters into the context, this is for external events (URLs)
        :param request:
        :param context:
        '''
        context['internal_events'] = False
        querySet, full_query_set = self.list_event_series(request)
        pages = TimetablesPaginator(querySet.select_related(
                                "owning_group",
                                "location"
                                    ). \
                                    prefetch_related(
                                "owning_group__owning_organisation",
                                "owning_group__term",
                                "owning_group__term__academic_year",
                                "associated_staff"
                                ),
                AllEvents.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))
        context[ContextKeys.PAGE] = page
        context[ContextKeys.CURRENT_PAGE] = page.number
        # construct object to use in template
        context["event_matches"] = []
        for event_serise in page.object_list:
            series_item = {}
            series_item['id'] = event_serise.id
            series_item['group_id'] = event_serise.owning_group.id
            series_item["external_timetable_url"] = event_serise.external_url
            series_item["level"] = self.get_level(event_serise)
            series_item["subject"] = self.get_subject(event_serise)
            
            context["event_matches"].append(series_item)

        context["event_matches_start"] = page.start_index()
        context["event_matches_end"] = page.end_index()
        context["event_matches_count"] = page.paginator.count

        self._load_filter_event_series(request, context, full_query_set)


    def _internal_events_context(self, request, context):
        context['internal_events'] = True
        querySet, full_query_set = self.list_events(request)
        pages = TimetablesPaginator(querySet.select_related(
                                "default_timeslot"
                                    ).prefetch_related(
                                "owning_series__owning_group",
                                "owning_series__owning_group__owning_organisation",
                                "owning_series__owning_group__term",
                                "owning_series__owning_group__term__academic_year",
                                "owning_series__location",
                                "owning_series__associated_staff"
                                ),
                AllEvents.results_per_page)
        page = pages.page_safe(request.GET.get(HttpParameters.PAGE, 1))
        context[ContextKeys.PAGE] = page
        context[ContextKeys.CURRENT_PAGE] = page.number

        # construct object to use in template
        context["event_matches"] = []
        for event in page.object_list:
            series_item = {}
            series_item['id'] = event.id
            series_item["title"] =  self.get_title(event)
            series_item["owning_group"] = self.get_owning_group(event)
            series_item["location"] = self.get_location(event)
            series_item["date"] = self.get_date(event)
            series_item["updated_at"] = self.get_last_update(event)
            series_item["timetable"] = self.get_classification(event)
            series_item["lecturers"] = self.get_lecturers(event)
            context["event_matches"].append(series_item)
        context["event_matches_start"] = page.start_index()
        context["event_matches_end"] = page.end_index()
        context["event_matches_count"] = page.paginator.count

        self._load_filter(request, context, full_query_set)



from timetables.utils.staticdata import create_yaml_loader_relative_to_file

load_yaml = create_yaml_loader_relative_to_file(__file__)

def page_overview_data():
    return load_yaml("uidata/overview.yaml")

def administrators():
    return load_yaml("uidata/administrators.yaml")

def administrators_add():
    return load_yaml("uidata/administrators-add.yaml")

def administrators_edit():
    return load_yaml("uidata/administrators-edit.yaml")

def faculties():
    return load_yaml("uidata/faculties.yaml")

def faculties_add():
    return load_yaml("uidata/faculties-add.yaml")

def faculties_edit():
    return load_yaml("uidata/faculties-edit.yaml")

def timetables():
    return load_yaml("uidata/timetables.yaml")

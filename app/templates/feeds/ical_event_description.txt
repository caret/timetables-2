{% load humanize %}
{{event.position1|ordinal}} event in the series {{series.title}}.

{% comment %}
Part of{% for group_usage in group_usages %}
{{ group_usage.organisation.name }} - {{ group_usage.levels|join:", " }} - {{ group_usage.subjects|join:", " }} - {{ group_usage.group.name }}:
{{ group_usage.group.description }}

{% endfor %}
{% endcomment %}
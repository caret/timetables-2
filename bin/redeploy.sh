#!/bin/sh
set -e
#
#
# Redeploys the app.
# Requires that the repo is setup so that git pull updates the code base
# the branch must be tracking the remote deployment branch
# git branch --set-upstream deployment origin/deployment
# Must be run the base of the application.
#
cat << SSHCONFIG >> ~/.ssh/config
Host=bitbucket.org
        user=git
        port=22
        Hostname=bitbucket.org
        IdentityFile=~/.ssh/deploy-key
SSHCONFIG
# Only update if there are changes, unfortunately git exits with non zero if there are.
set +e
UPTO_DATE=`git pull | grep "Already up-to-date." `
set -e
if [ "$UPTO_DATE" = "Already up-to-date." ]
then
  echo "Upto date, no action necessary "
  exit
fi
python bin/checkdependencies.py
git log | head -4  > deploy.txt
set +e
git status | head -1 >> deploy.txt
set -e
git show --pretty=oneline HEAD | cut -c1-41 | head -1  > version.txt
# Fix permissions
find . -type d -exec chmod 775 {} \;
find . -type f -exec chmod 664 {} \;
pushd app/timetables
python manage.py validate
python manage.py syncdb
python manage.py migrate
python manage.py collectstatic --noinput -v 0
popd
sudo chown -R root:root .
sudo chown -R www-data:root app-data
sudo apachectl graceful
echo "Apache Restarted Gracefully All Done."

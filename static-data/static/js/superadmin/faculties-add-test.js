define(["jquery",
        "jquery-ui/dialog"
    ], 
function(){
	// click listener for "new classifier" buttons
	var set_add_new_listeners = function() {
		$( "button.add-new" ).click( function(){
			$( "#classifier_type").val( "classifier" );
			
			$( "#group_alias" ).val( $( this ).prev().val() );
			$( "#form_classifier label" ).text( "Classifier name" );
			$( "input#dialog_name" ).val( "" );
			
			$( "#form_dialog" ).dialog({
				title: "Add New Classifier"
			}).dialog( "open" );
			
			return false;
		});
	};
	
	// click listener for "delete classifier" links
	var set_del_listeners = function() {
		$( "a.classifier_delete" ).click( function(){
			// test whether to show the "click add new" message (all of the classifiers have been removed)
			if( $(this).parent().parent().find( "p" ).length <= 2 )
			{
				$(this).parent().parent().find( "p" ).show();
			}
			
			// remove this classifier
			$( this ).parent().remove();
			
			return false;
		});
	};

	// initialise the page - called when page is loaded
	var initialise = function() {
		$.each( classifier_groups, function( alias, group ) {
			url = "/superadministration/faculties/add/f/classifier-group/";
			title = group["title"]

			// create classifier group
			ajax_add_classifier_group( url, title, alias );
			
			// create group's classifiers
			url = "/superadministration/faculties/add/f/classifier/";
			$.each( group["classifiers"], function( id, classifier ){
				ajax_add_classifier( url, classifier, alias );
			});
		});
	}
	
	// add new classifier group - keep as separate function so it may be called for initialisation
	var ajax_add_classifier_group = function( url, title, alias ) {
		$.post( url, { id: 0, title: title, alias: alias }, function(html){
			$( "div#classifier-groups" ).append( html );
			set_add_new_listeners();
		});
	};
	
	// add new classifier - keep as separate function so it may be called for initialisation
	var ajax_add_classifier = function( url, title, alias ) {
		$.post( url, { id: 0, title: title, alias: alias }, function(html){
			$( "div#cl_group_"+alias+" p.when-empty" ).hide();
			$( "div#cl_group_"+alias ).append( html );
			set_del_listeners();
		});
	}
	
	// dialog form save
	var save = function() {
		var type = $( "#classifier_type" ).val();
		
		url = "/superadministration/faculties/add/f/"+type+"/"; // ugly - is there a way to get this URL form Django by name?
		title = $( "input#dialog_name" ).val().trim();
		
		if( title == '' ) // TODO - provide error message
		{
			return false;
		}
		
		if( type == 'classifier-group' ){ // classifier group
			alias = classifier_groups.length;
			classifier_groups[alias] = {
				"id": 0,
				"title": title,
				"classifiers": []
			};
			ajax_add_classifier_group( url, title, alias );
		} else { // classifier
			alias = $( "#group_alias" ).val();
			classifier_groups[alias]["classifiers"].push({
				"id": 0,
				"title": title
			});
			ajax_add_classifier( url, title, alias );
		}
		
		$( "#form_dialog" ).dialog( "close" );
	};
	
	// "add stuff" dialog - used for classifier groups and classifiers
	$( "#form_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Cancel: function() {
				$( this ).dialog( "close" );
			},
			"Save": function() {
				save();
			}
		},
		close: function() {}
	});
	
	// new classifier group
	$( "#add-classifier-group-button" ).click( function() {
		$( "#classifier_type").val( "classifier-group" );
		$( "#form_classifier label" ).text( "Group name" );
		$( "input#dialog_name" ).val( "" );
		
		$( "#form_dialog" ).dialog({
			title: "Add New Classifier Group"
		}).dialog( "open" );
		
		return false;
	});
	
	// initialise the page
	initialise();
	
	// prevent dialog form from submitting itself
	$( "form#form_classifier" ).submit( function(){
		save();
		return false;
	});
	
	// validate add faculty form
	var form_check = function( form_names ) {
		var valid = true;

		// get year
		var year = $( "#id_year" ).val();
		year = $( "#id_year option[value='"+year+"']" ).text();
		
		// process input values
		$.each( form_names, function( index, form_name ){
			var value = $( "#id_"+form_name ).val().trim().toLowerCase();

			// test faculty name
			var error = "";
			if( $.inArray( value, faculties_exist[year][form_name] ) > -1 ){
				error = "This value has already been used.";
			}
			if( value == "" ){
				error = "This field is required.";
			}
			if( error != "" ){
				valid = false;
				$( "#li_id_"+form_name ).addClass( "invalid-content" );
				$( "#id_"+form_name+"_error li" ).text( error ).parent().slideDown(250);
			}
			
			// clear any existing error warnings - do after error checking to avoid graphical bounce as list slides
			if( valid ){
				$( "#li_id_"+form_name ).removeClass( "invalid-content" );
				$( "#id_"+form_name+"_error" ).hide();
			}
		});
		 
		// return whether the form contains valid data
		return valid;
	};
	
	// form error checking
	$( "#id_name" ).keyup( function(){
		form_check(["name"]);
	});
	$( "#id_code" ).keyup( function(){
		form_check(["code"]);
	});
	$( "#id_year" ).change( function(){
		form_check(["name", "code"]);
	});
	$( "#form_add_faculty" ).submit( function(){
		return form_check(["name", "code"]);
	});
});
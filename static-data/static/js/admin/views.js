define(["jquery",
        "domReady",
        "jquery-ui/dialog",
        "jquery-ui/datepicker",
        "jquery-ui/core", // from here on are the requirements for the select box widget - may be best to move this to a separate includable file
        "jquery-ui/widget",
        "jquery-ui/position",
        "jquery-ui/autocomplete",
        "jquery-ui/button",
        "jquery-ui/selectbox"
        ], 
        function($) {
    "use strict";
    
    var EnableTopNav = function enableTopNav( getUrlFunc ) {
        /*
         * Activates the drop downs on the top nav area,
         * that will use the function to generate a URL to which 
         * the page will redirect when the dropdowns are changed.
         * To use include admin/views in the requireJS define and call
         * views.enableTopNav(func) where func returns the URL to load.
         */
        // Bind in the organization / year control
        var reload_page = function() {
            var start_year = $('#select-date').val();
            var org_code = $('#select-subject').val();
            window.location = getUrlFunc(start_year, org_code);
        };
        // make certain on load the selector value matches the 
        // loaded value to avoid problems with the back button
        $('#select-subject').val($('#select-subject-initial').val());
        $('#select-date').val($('#select-date-initial').val());
        
        $('#select-subject').change(reload_page);
        $('#select-date').change(reload_page);
    };
    
    /* ----------------------------------------------------------------------- */
    /* Error dialog */
    /* ----------------------------------------------------------------------- */
    $("#error-dialog").dialog({
        autoOpen: false,
        width: 400,
        modal: true
    });

    $('#error-dialog .close-dialog').click(function() {
        $("#error-dialog").dialog("close");
    });

    // make notifications show and then fade
    $(".after_post_notification").dialog({
        autoOpen: false,
        hide: 'fade',
        width: 400,
        modal: true
    });

    $('.after_post_notification .close-dialog').click(function() {
        $(this).parents('.after_post_notification').dialog("close");
    });

    $('.after_post_notification').dialog('open');
    setTimeout(function() {
        $('.after_post_notification').dialog('close');
    }, 2000);


    // bind a date picker to everything that is a date picker.
    $(".date-picker-control").datepicker({ dateFormat: "dd/mm/yy" });
 
    
    // set up JQuery selectors for date and year
    $( "#select-date" ).selectbox();
    $( "#select-subject" ).selectbox();
    
    return {
        enableTopNav: EnableTopNav
    };
});
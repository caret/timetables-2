define(["jquery",
        "admin/validation",
        "admin/views",
        "admin/classification-selectors"
        ],
        function($, validation, views){    
	"use strict";

    views.enableTopNav(function( start_year, org_code ){
        return "/administration/" +
            encodeURIComponent(start_year) +
            "/" + encodeURIComponent(org_code) +
            "/administrators/edit";
    });
    
    $("#disable-admin").click(function() {
    	if ( $("#disable-admin-value").val() === "0" ) {
    		$("#disable-admin-value").val("1");
    		$("#disable-admin-message").show();
    		$("#disable-admin").hide();
    		$("#enable-admin").show();
    	}
    });
    $("#enable-admin").click(function() {
    	if ( $("#disable-admin-value").val() === "1" ) {
    		$("#disable-admin-value").val("0");
    		$("#disable-admin-message").hide();
    		$("#enable-admin").hide();
    		$("#disable-admin").show();
    	}
    });
    
    validation.bind('#add-administrators'); // lots of hassle to use edit-administrators here see the css.
    
    return {};
});